EESchema Schematic File Version 4
LIBS:partA-cache
EELAYER 26 0
EELAYER END
$Descr User 5500 4000
encoding utf-8
Sheet 6 8
Title "Experiment 2 Circuit 2"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT?
U 1 1 5BAA727B
P 1600 1770
F 0 "BT?" H 1721 1867 50  0000 L CNN
F 1 "Battery_Cell" H 1721 1774 50  0000 L CNN
F 2 "" V 1600 1830 50  0001 C CNN
F 3 "~" V 1600 1830 50  0001 C CNN
	1    1600 1770
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 5BAA7444
P 2250 1250
F 0 "SW?" H 2250 1491 50  0000 C CNN
F 1 "SW_SPST" H 2250 1398 50  0000 C CNN
F 2 "" H 2250 1250 50  0001 C CNN
F 3 "" H 2250 1250 50  0001 C CNN
	1    2250 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA?
U 1 1 5BAA7677
P 3710 1450
F 0 "LA?" H 3841 1497 50  0000 L CNN
F 1 "Lamp" H 3841 1404 50  0000 L CNN
F 2 "" V 3710 1550 50  0001 C CNN
F 3 "~" V 3710 1550 50  0001 C CNN
	1    3710 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA?
U 1 1 5BAA7712
P 3710 1850
F 0 "LA?" H 3841 1897 50  0000 L CNN
F 1 "Lamp" H 3841 1804 50  0000 L CNN
F 2 "" V 3710 1950 50  0001 C CNN
F 3 "~" V 3710 1950 50  0001 C CNN
	1    3710 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA?
U 1 1 5BAA77B0
P 3710 2250
F 0 "LA?" H 3841 2297 50  0000 L CNN
F 1 "Lamp" H 3841 2204 50  0000 L CNN
F 2 "" V 3710 2350 50  0001 C CNN
F 3 "~" V 3710 2350 50  0001 C CNN
	1    3710 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 1250 3710 1250
Wire Wire Line
	2050 1250 1600 1250
Wire Wire Line
	1600 1250 1600 1570
Wire Wire Line
	1600 1870 1600 2450
Wire Wire Line
	1600 2450 3710 2450
$EndSCHEMATC
