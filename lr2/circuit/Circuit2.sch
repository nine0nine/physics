EESchema Schematic File Version 4
LIBS:partA-cache
EELAYER 26 0
EELAYER END
$Descr User 5500 4000
encoding utf-8
Sheet 3 8
Title "Experiment 1 Circuit 2"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT?
U 1 1 5BA29862
P 1680 1680
F 0 "BT?" H 1801 1777 50  0000 L CNN
F 1 "Battery_Cell" H 1801 1684 50  0000 L CNN
F 2 "" V 1680 1740 50  0001 C CNN
F 3 "~" V 1680 1740 50  0001 C CNN
	1    1680 1680
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA?
U 1 1 5BA29A19
P 3670 1950
F 0 "LA?" H 3801 1997 50  0000 L CNN
F 1 "Lamp" H 3801 1904 50  0000 L CNN
F 2 "" V 3670 2050 50  0001 C CNN
F 3 "~" V 3670 2050 50  0001 C CNN
	1    3670 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA?
U 1 1 5BA29B71
P 3670 1400
F 0 "LA?" H 3801 1447 50  0000 L CNN
F 1 "Lamp" H 3801 1354 50  0000 L CNN
F 2 "" V 3670 1500 50  0001 C CNN
F 3 "~" V 3670 1500 50  0001 C CNN
	1    3670 1400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x01 SW?
U 1 1 5BA29D96
P 2560 1130
F 0 "SW?" H 2560 1400 50  0000 C CNN
F 1 "SW_DIP_x01" H 2560 1307 50  0000 C CNN
F 2 "" H 2560 1130 50  0001 C CNN
F 3 "" H 2560 1130 50  0001 C CNN
	1    2560 1130
	1    0    0    -1  
$EndComp
Wire Wire Line
	1680 1470 1680 1130
Wire Wire Line
	1680 1130 2260 1130
Wire Wire Line
	2860 1130 3670 1130
Wire Wire Line
	3670 1130 3670 1200
Wire Wire Line
	3670 1600 3670 1750
Wire Wire Line
	3670 2150 3670 2200
Wire Wire Line
	3670 2200 1680 2200
Wire Wire Line
	1680 2200 1680 1780
$EndSCHEMATC
