EESchema Schematic File Version 4
LIBS:partA-cache
EELAYER 26 0
EELAYER END
$Descr User 5500 4000
encoding utf-8
Sheet 4 8
Title "Experiment 1 Circuit 3"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT?
U 1 1 5BA29FF0
P 1540 1800
F 0 "BT?" H 1661 1897 50  0000 L CNN
F 1 "Battery_Cell" H 1661 1804 50  0000 L CNN
F 2 "" V 1540 1860 50  0001 C CNN
F 3 "~" V 1540 1860 50  0001 C CNN
	1    1540 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA?
U 1 1 5BA2A1DA
P 2750 1730
F 0 "LA?" H 2881 1777 50  0000 L CNN
F 1 "Lamp" H 2881 1684 50  0000 L CNN
F 2 "" V 2750 1830 50  0001 C CNN
F 3 "~" V 2750 1830 50  0001 C CNN
	1    2750 1730
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA?
U 1 1 5BA2A274
P 3600 1720
F 0 "LA?" H 3731 1767 50  0000 L CNN
F 1 "Lamp" H 3731 1674 50  0000 L CNN
F 2 "" V 3600 1820 50  0001 C CNN
F 3 "~" V 3600 1820 50  0001 C CNN
	1    3600 1720
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x01 SW?
U 1 1 5BA2A38A
P 2080 1340
F 0 "SW?" H 2080 1610 50  0000 C CNN
F 1 "SW_DIP_x01" H 2080 1517 50  0000 C CNN
F 2 "" H 2080 1340 50  0001 C CNN
F 3 "" H 2080 1340 50  0001 C CNN
	1    2080 1340
	1    0    0    -1  
$EndComp
Wire Wire Line
	1540 1600 1540 1340
Wire Wire Line
	1540 1340 1780 1340
Wire Wire Line
	2380 1340 2750 1340
Wire Wire Line
	3600 1340 3600 1520
Wire Wire Line
	3600 1920 3600 2100
Wire Wire Line
	3600 2100 2750 2100
Wire Wire Line
	1540 2100 1540 1900
Wire Wire Line
	2750 1930 2750 2100
Connection ~ 2750 2100
Wire Wire Line
	2750 2100 1540 2100
Wire Wire Line
	2750 1530 2750 1340
Connection ~ 2750 1340
Wire Wire Line
	2750 1340 3600 1340
$EndSCHEMATC
