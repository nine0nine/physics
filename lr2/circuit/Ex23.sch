EESchema Schematic File Version 4
LIBS:partA-cache
EELAYER 26 0
EELAYER END
$Descr User 5500 4000
encoding utf-8
Sheet 7 8
Title "Experiment 2 Circuit 3"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT?
U 1 1 5BAA7CDC
P 1670 2010
F 0 "BT?" H 1791 2107 50  0000 L CNN
F 1 "Battery_Cell" H 1791 2014 50  0000 L CNN
F 2 "" V 1670 2070 50  0001 C CNN
F 3 "~" V 1670 2070 50  0001 C CNN
	1    1670 2010
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 5BAA7EA4
P 2470 1590
F 0 "SW?" H 2470 1831 50  0000 C CNN
F 1 "SW_SPST" H 2470 1738 50  0000 C CNN
F 2 "" H 2470 1590 50  0001 C CNN
F 3 "" H 2470 1590 50  0001 C CNN
	1    2470 1590
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA1
U 1 1 5BAA7F63
P 3060 2000
F 0 "LA1" H 3191 2047 50  0000 L CNN
F 1 "Lamp" H 3191 1954 50  0000 L CNN
F 2 "" V 3060 2100 50  0001 C CNN
F 3 "~" V 3060 2100 50  0001 C CNN
	1    3060 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA3
U 1 1 5BAA8056
P 3790 2000
F 0 "LA3" H 3921 2047 50  0000 L CNN
F 1 "Lamp" H 3921 1954 50  0000 L CNN
F 2 "" V 3790 2100 50  0001 C CNN
F 3 "~" V 3790 2100 50  0001 C CNN
	1    3790 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA2
U 1 1 5BAA8090
P 3410 1590
F 0 "LA2" V 3681 1590 50  0000 C CNN
F 1 "Lamp" V 3588 1590 50  0000 C CNN
F 2 "" V 3410 1690 50  0001 C CNN
F 3 "~" V 3410 1690 50  0001 C CNN
	1    3410 1590
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1670 1810 1670 1590
Wire Wire Line
	1670 1590 2270 1590
Wire Wire Line
	2670 1590 3060 1590
Wire Wire Line
	3610 1590 3790 1590
Wire Wire Line
	3790 1590 3790 1800
Wire Wire Line
	3060 2200 3790 2200
Wire Wire Line
	3060 2200 1670 2200
Wire Wire Line
	1670 2200 1670 2110
Connection ~ 3060 2200
Wire Wire Line
	3060 1800 3060 1590
Connection ~ 3060 1590
Wire Wire Line
	3060 1590 3210 1590
$EndSCHEMATC
