EESchema Schematic File Version 4
LIBS:partA-cache
EELAYER 26 0
EELAYER END
$Descr User 5500 4000
encoding utf-8
Sheet 2 8
Title "Experiment 1 Circuit 1"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT?
U 1 1 5BA295FB
P 1430 1840
F 0 "BT?" H 1551 1937 50  0000 L CNN
F 1 "Battery_Cell" H 1551 1844 50  0000 L CNN
F 2 "" V 1430 1900 50  0001 C CNN
F 3 "~" V 1430 1900 50  0001 C CNN
	1    1430 1840
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA?
U 1 1 5BA296F3
P 3730 1790
F 0 "LA?" H 3861 1837 50  0000 L CNN
F 1 "Lamp" H 3861 1744 50  0000 L CNN
F 2 "" V 3730 1890 50  0001 C CNN
F 3 "~" V 3730 1890 50  0001 C CNN
	1    3730 1790
	1    0    0    -1  
$EndComp
Wire Wire Line
	1430 1640 1430 1420
Wire Wire Line
	1430 1420 3730 1420
Wire Wire Line
	3730 1420 3730 1590
Wire Wire Line
	3730 1990 3730 2180
Wire Wire Line
	3730 2180 1430 2180
Wire Wire Line
	1430 2180 1430 1940
$EndSCHEMATC
