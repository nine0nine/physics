EESchema Schematic File Version 4
LIBS:partA-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 560  560  2640 2000
U 5BA295D0
F0 "PartA1" 50
F1 "Circuit1.sch" 50
$EndSheet
$Sheet
S 3320 580  3030 1920
U 5BA295D3
F0 "PartA2" 50
F1 "Circuit2.sch" 50
$EndSheet
$Sheet
S 6500 570  3450 2280
U 5BA295DD
F0 "PartA3" 50
F1 "Circuit3.sch" 50
$EndSheet
$Sheet
S 1370 3290 1360 1030
U 5BAA5889
F0 "Ex21" 50
F1 "Ex21.sch" 50
$EndSheet
$Sheet
S 3270 3220 1570 1090
U 5BAA70C7
F0 "Ex2" 50
F1 "Ex22.sch" 50
$EndSheet
$Sheet
S 5590 3280 1580 1030
U 5BAA7CBE
F0 "Ex23" 50
F1 "Ex23.sch" 50
$EndSheet
$Sheet
S 7700 3270 1330 1020
U 5BAA8BA3
F0 "Ex24" 50
F1 "Ex24.sch" 50
$EndSheet
$EndSCHEMATC
