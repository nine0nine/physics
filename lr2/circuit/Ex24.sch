EESchema Schematic File Version 4
LIBS:partA-cache
EELAYER 26 0
EELAYER END
$Descr User 5500 4000
encoding utf-8
Sheet 8 8
Title "Experiment 2 Circuit 4"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Lamp LA2
U 1 1 5BAA8BBA
P 3230 1720
F 0 "LA2" H 3361 1767 50  0000 L CNN
F 1 "Lamp" H 3361 1674 50  0000 L CNN
F 2 "" V 3230 1820 50  0001 C CNN
F 3 "~" V 3230 1820 50  0001 C CNN
	1    3230 1720
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA3
U 1 1 5BAA8CE5
P 3790 1720
F 0 "LA3" H 3921 1767 50  0000 L CNN
F 1 "Lamp" H 3921 1674 50  0000 L CNN
F 2 "" V 3790 1820 50  0001 C CNN
F 3 "~" V 3790 1820 50  0001 C CNN
	1    3790 1720
	1    0    0    -1  
$EndComp
$Comp
L Device:Lamp LA1
U 1 1 5BAA8D2B
P 2800 1300
F 0 "LA1" V 2529 1300 50  0000 C CNN
F 1 "Lamp" V 2622 1300 50  0000 C CNN
F 2 "" V 2800 1400 50  0001 C CNN
F 3 "~" V 2800 1400 50  0001 C CNN
	1    2800 1300
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 5BAA8E57
P 2160 1300
F 0 "SW?" H 2160 1541 50  0000 C CNN
F 1 "SW_SPST" H 2160 1448 50  0000 C CNN
F 2 "" H 2160 1300 50  0001 C CNN
F 3 "" H 2160 1300 50  0001 C CNN
	1    2160 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery_Cell BT?
U 1 1 5BAA8ECA
P 1460 2130
F 0 "BT?" H 1581 2227 50  0000 L CNN
F 1 "Battery_Cell" H 1581 2134 50  0000 L CNN
F 2 "" V 1460 2190 50  0001 C CNN
F 3 "~" V 1460 2190 50  0001 C CNN
	1    1460 2130
	1    0    0    -1  
$EndComp
Wire Wire Line
	2360 1300 2600 1300
Wire Wire Line
	1460 2230 3230 2230
Wire Wire Line
	3790 1300 3790 1520
Wire Wire Line
	3230 1520 3230 1300
Wire Wire Line
	3790 2230 3790 1920
Wire Wire Line
	3230 1920 3230 2230
Connection ~ 3230 2230
Wire Wire Line
	3230 2230 3790 2230
Wire Wire Line
	1460 1300 1460 1930
Wire Wire Line
	1460 1300 1960 1300
Wire Wire Line
	3000 1300 3230 1300
Connection ~ 3230 1300
Wire Wire Line
	3230 1300 3790 1300
$EndSCHEMATC
